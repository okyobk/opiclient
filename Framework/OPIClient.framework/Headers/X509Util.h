//
//  X509Util.h
//  Voffice2.1
//
//  Created by VTIT on 10/20/14.
//
//

#import <Foundation/Foundation.h>
#import "JNKeychain.h"

@interface X509Util : NSObject
+ (NSString *) createX509CsrWithX509Name:(NSString*)path password:(NSString *)pw X509Name:(NSString *)name;
+ (NSData *) createX509Csr:(NSString*)path password:(NSString *)pw CN:(NSString*)CN C:(NSString *)C ST:(NSString *)ST L:(NSString *)L O:(NSString *)O E:(NSString *)E;
+ (BOOL) createPkcs12:(NSData *)crt path:(NSString *)path Pkcs8Path:(NSString *)pkcs8 user:(NSString *)usr password:(NSString *)pw;

+ (NSData *) signRaw:(NSString *)path password:(NSString *)pw content:(NSData*)content;
+ (NSData *) signRawUseIosApi:(NSString *)path password:(NSString *)pw content:(NSData*)content;

+ (BOOL) changePkcs12Password:(NSString *)path oldpass:(NSString *)oldpass newpass:(NSString *) newpass;
+ (BOOL) changeAesKey:(NSString *)path oldpass:(NSData *)oldkey newpass:(NSData *) newkey;
// Util
+ (NSString *) HexToString:(NSData *)data;
+ (NSData *) StringToHex:(NSString *)data;

//+ (NSString*) getPathFilePKCS8;
//+ (NSString*) getPathFilePKCS12;
////+ (NSString*) getPathFileKeyStore;
//+ (NSString*) getLocalDataPath;
//
//+ (void) backupFilePKCS12;
//+ (void) restoreFilePKCS12;
//+ (void) completeChangeFilePKCS12;
//+ (void) deleteAllData;

+ (NSString*) getPathFilePKCS8WithId : (NSString * ) strId;
+ (NSString*) getPathFilePKCS12WithId : (NSString * ) strId;
//+ (NSString*) getPathFileKeyStore;
+ (NSString*) getLocalDataPathWithId : (NSString * ) strId;

+ (void) backupFilePKCS12WithId : (NSString * ) strId;
+ (void) restoreFilePKCS12WithId : (NSString * ) strId;
+ (void) completeChangeFilePKCS12WithId : (NSString * ) strId;
+ (void) deleteAllDataWithId : (NSString * ) strId;

+ (NSMutableDictionary *) parseX509Name:(NSString *)x509name;
+ (NSString *) getPublicKeyFromPKCS8:(NSString *)path password:(NSString *)pw;

//dainv5 18/05/2015
+ (NSData *)dataFromHexString:(NSString *)string;
+ (NSData*) PKCSSignBytesSHA1withRSA:(NSData*) plainData privateKey:(SecKeyRef)privateKey;
+ (NSData*) PKCSSignBytesSHA256withRSA:(NSData*) plainData privateKey:(SecKeyRef)privateKey;
+ (NSData *) PaddingPkcs1Sha1:(NSData *) data;

+ (NSData *) signRawUseIosLikeSIMApi:(NSString *)path password:(NSString *)pw content:(NSData*)content;
+ (NSData *)signRawUseIosLikeSIMApi:(NSString *)path password:(NSString *)pw content:(NSData *)content withPrivateKey:(SecKeyRef)privateKey;
+ (SecKeyRef)x509ExtractPrivateKeyFromPkcs12:(NSString *)keyPath password:(NSString *)password;
//end

// for test
//+ (void) saveKeyStore:(NSString *)keyPath outPath:(NSString *)outPath sessionKey:(NSData *)sessionKey;
//+ (void) saveKeyStoreFromMemory:(NSData *)keyData outPath:(NSString *)outPath sessionKey:(NSData *)sessionKey;
@end
