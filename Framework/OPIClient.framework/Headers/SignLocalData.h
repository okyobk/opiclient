//
//  SignLocalData.h
//  X509Test
//
//  Created by VTIT on 10/30/14.
//  Copyright (c) 2014 VTIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JNKeychain.h"

@interface SignLocalData : NSObject

//- (id) initWithContentsOfFile:(NSString *)path;
//- (id) init:(NSString *)path user:(NSString *)user publickey:(NSString *)publickey;

- (id) initWithKeyChain : (NSString *) keyChain;
- (id) initWithUser:(NSString *)user publickey:(NSString *)publickey andKeyChain : (NSString *) keyChain;

- (BOOL) checkUser:(NSString *)user;
- (NSString *) getPublicKey;

- (BOOL) setState: (NSString *)state andKeyChain : (NSString *) keyChain;
//- (BOOL) setState: (NSString *)state;

- (NSString *) getState;


- (BOOL) setSerial:(NSString *)serial publickey:(NSString *)publickey andKeyChain : (NSString *) keyChain;
//- (BOOL) setSerial:(NSString *)serial publickey:(NSString *)publickey;
- (NSString *) getSerial;


-(void)resetPublickey;


@end
